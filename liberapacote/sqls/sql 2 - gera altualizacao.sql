﻿-- Table: atualizacao

-- DROP TABLE atualizacao;

CREATE TABLE atualizacao
(
  i_cdatualizacao serial NOT NULL,
  c_descricao character varying(50),
  i_versao integer,
  i_cdsolicitacao integer NOT NULL,
  f_tipo_acao character(1) NOT NULL DEFAULT 'B'::bpchar,
  d_criacao timestamp without time zone NOT NULL,
  d_liberacao timestamp without time zone,
  i_cdprogramador integer,
  i_cdtestador integer,
  c_sql_retorno text,
  c_sql text,
  i_cdprograma integer,
  c_path character varying(100),
  o_arquivo oid,
  c_aviso character varying(1024),
  c_area_resp character varying(6),
  f_status character(1) NOT NULL DEFAULT 'N'::bpchar,
  c_nomearquivo character varying(50),
  d_arquivo timestamp without time zone,
  i_cdprojeto integer,
  i_versao_vinculada integer,
  i_cdfunc_liberou integer,
  f_atualiza_banco_age character(1) NOT NULL DEFAULT 'N'::bpchar,
  f_copiado character varying(1) DEFAULT 'N'::character varying,
  f_controle_menu character varying(1) DEFAULT 'N'::character varying,
  f_status_sol character(1),
  c_descricao_sol character varying(2048),
  c_titulo_sol character varying(100),
  c_resultadosql character varying(150),
  d_atualizacaosql timestamp with time zone,
  f_automatico character varying(1) DEFAULT 'N'::character varying,
  c_nmprograma character varying(50),
  CONSTRAINT atualizacao_pkey PRIMARY KEY (i_cdatualizacao),
  CONSTRAINT atualizacao_i_versao_key UNIQUE (i_versao),
  CONSTRAINT atualizacao_f_atualiza_banco_age_check CHECK (f_atualiza_banco_age = ANY (ARRAY['N'::bpchar, 'S'::bpchar])),
  CONSTRAINT atualizacao_f_controle_menu_check CHECK (f_controle_menu::bpchar = ANY (ARRAY['N'::bpchar, 'I'::bpchar, 'E'::bpchar])),
  CONSTRAINT atualizacao_f_tipo_acao_check CHECK (f_tipo_acao = ANY (ARRAY['T'::bpchar, 'F'::bpchar, 'A'::bpchar, 'C'::bpchar, 'P'::bpchar, 'M'::bpchar, 'U'::bpchar, 'B'::bpchar, 'R'::bpchar, 'O'::bpchar, 'V'::bpchar, 'G'::bpchar, 'S'::bpchar]))
)
WITH (
  OIDS=FALSE
);
ALTER TABLE atualizacao
  OWNER TO postgres;
