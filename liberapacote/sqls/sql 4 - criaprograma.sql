﻿-- Function: CriaPrograma_Menu

-- DROP FUNCTION CriaPrograma_Menu(varchar,varchar,varchar,varchar,varchar,varchar,varchar,int,int);

CREATE OR REPLACE FUNCTION CriaPrograma_Menu(varchar,varchar,varchar,varchar,varchar,varchar,varchar,int,int)
  RETURNS varchar AS
$BODY$
/* Alterado por    :                         em __/__/____ */
DECLARE   
vNomePrograma      Alias For $1;
vDescricaoPrograma Alias For $2;
vHintPrograma      Alias For $3;
vProgHeranca       Alias For $4;
vNomeMenu          Alias For $5;
vTituloMenu        Alias For $6;
vMenuPai	   Alias For $7;
vI_Ordem	   Alias For $8;
vI_Nivel	   Alias For $9;
vIdPrograma	   integer;
vIdProgramaHeranca integer;
vIdMenuPai	   integer;
vRetorno	   varchar;
Begin
	vIdPrograma := 0 ;
	vRetorno :='';
	select i_cdprograma into vIdPrograma from wv.programa where upper(c_nmprograma)= upper(vNomePrograma);

	If  vIdPrograma is null Then
		select i_cdprograma into vIdProgramaHeranca from wv.programa where upper(c_nmprograma)= upper(vProgHeranca);
		
		INSERT INTO wv.programa(c_nmprograma, c_descricaoprograma, c_hintprograma,i_cdheranca)  VALUES (vNomePrograma,vDescricaoPrograma,vHintPrograma,vIdProgramaHeranca);

		select i_cdmenu into vIdMenuPai from wv.menu where upper(c_nomemenu)= upper(vMenuPai);

		select i_cdprograma into vIdPrograma from wv.programa where upper(c_nmprograma)= upper(vNomePrograma);
		
		INSERT INTO wv.menu(c_nomemenu, c_hintmenu, i_nrnivel, i_cdprograma, i_ordem,f_programa, i_cdmenupai) VALUES (vNomeMenu, vTituloMenu, vI_Nivel, vIdPrograma,vI_Ordem,'S',vIdMenuPai);

		vRetorno := 'Foi Inserido o programa e o menu com sucesso!'; 
	Else
		vRetorno := 'Programa Já Cadastrado!';
	End If;

  RETURN vRetorno;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;
ALTER FUNCTION CriaPrograma_Menu(varchar,varchar,varchar,varchar,varchar,varchar,varchar,int,int) OWNER TO postgres;

-- exemplo ==> select CriaPrograma_Menu('F_requisicaocompra','programa de requicao de compra','F_requisicaocompra','f_padraocadastro','requisicaocompra','Requisição de Compra','Compras',7,1);